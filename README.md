`topicName` needs to be updated to produce message to different topic 

`swishRawMarketTemplate.json` needs to be updated with the message which is expected by the new topic

`BOOTSTRAP_SERVERS_CONFIG` needs to be updated in case you want to produce message for dev environment. Current version is talking with nonProd

check out `message` method in `MlbMarketUpdate` object in case you want to override different variable inside the json (current version overwrites `fixtureId` only)

`don't forget to check feed and exec methods to overwrite session`

and check `execute` method in `KafkaProducerAction` class in case you want to get the value from session that you set in `Simulation`
