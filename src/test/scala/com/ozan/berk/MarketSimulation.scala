package com.ozan.berk

import com.ozan.berk.kafka.KafkaProtocol
import com.ozan.berk.kafka.consumer.KafkaConsumerAction
import com.ozan.berk.kafka.producer.KafkaProducerAction
import com.ozan.berk.util.MlbMarketUpdate
import io.gatling.core.Predef._
import io.gatling.core.action.Action
import io.gatling.core.action.builder.ActionBuilder
import io.gatling.core.structure.{ScenarioBuilder, ScenarioContext}

import scala.concurrent.duration._

class MarketSimulation extends Simulation {

  private val produceMarkets: ActionBuilder = (ctx: ScenarioContext, next: Action) => {
    new KafkaProducerAction(ctx, ctx.throttled, next)
  }

  private val consumeMarkets: ActionBuilder = (ctx: ScenarioContext, next: Action) => {
    new KafkaConsumerAction(ctx, ctx.throttled, next)
  }

  val kafkaProtocol: KafkaProtocol = KafkaProtocol()

  val marketCreation: ScenarioBuilder = scenario("Market Creation")
    .feed(Iterator.continually(Map("fixtureId" -> java.util.UUID.randomUUID.toString)))
    .exec(session => session.set("event", MlbMarketUpdate.message(session("fixtureId").as[String])))
    .exec(produceMarkets)
    .exec(consumeMarkets)

  setUp(
    marketCreation.inject(constantUsersPerSec(10) during (1 minutes))
  ).protocols(kafkaProtocol)
}