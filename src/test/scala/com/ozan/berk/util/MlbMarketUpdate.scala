package com.ozan.berk.util

object MlbMarketUpdate {
  def message(fixtureId: String): String = {
    val update = JsonUtil.readFile("swishRawMarketTemplate.json").replace("{{fixtureId}}", fixtureId)
    update
  }
}